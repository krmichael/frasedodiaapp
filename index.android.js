import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, AppRegistry, Alert } from 'react-native';

const gerarNovaFrase = () => {

  let numeroAleatorio = Math.random();
      numeroAleatorio = Math.floor(numeroAleatorio * 5);

  let frases = Array();
      frases[0] = 'É melhor conquistar a si mesmo do que vencer mil batalhas.';
      frases[1] = 'O entusiasmo é a maior força da alma. Conserva-o e nunca te faltará poder para conseguires o que desejas.';
      frases[2] = 'Apenas é igual a outro quem prova sê-lo e apenas é digno da liberdade quem a sabe conquistar.';
      frases[3] = 'Você nasceu para vencer, mas para ser um vencedor você precisa planejar para vencer, se preparar para vencer, e esperar vencer.';
      frases[4] = 'Sofrer, é só uma vez; vencer, é para a eternidade.';

  let fraseEscolhida = frases[numeroAleatorio];

  Alert.alert('Frase do dia!!',fraseEscolhida);
}

export class App extends Component {

  render() {

    const { root, botao, textoBotao } = styles;

    return (
      <View style={root}>
        <Image source={require('./img/logo.png')}/>
        <TouchableOpacity
          onPress={gerarNovaFrase}
          style={botao}>
          <Text style={textoBotao}>Frases do dia!</Text>
        </TouchableOpacity>
      </View>
    );
  }
};

const styles = {
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  botao: {
    backgroundColor: '#538530',
    paddingVertical: 10,
    paddingHorizontal: 30,
    marginTop: 10,
    borderRadius: 5
  },
  textoBotao: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20
  }
};

AppRegistry.registerComponent('app2', () => App);